// [ 647 ] Given a string, your task is to count how many palindromic substrings in this string.

import (
    "strings"
)

func Min(x, y int) int {
    if x<y { return x}
    return y
}

func manach(s string) []int {
    A := "@%" + strings.Join(strings.Split(s, ""), "%") + "%$"
    var Z []int

    Z = make([]int, len(A))

    center, right := 0, 0
    for i:=1; i<len(A)-1; i++ {
        if i < right {
            Z[i] = Min(right-i, Z[center-(i-center)])
        }
        for A[i+Z[i]+1] == A[i-Z[i]-1] {
            Z[i]++
        }
        if i+Z[i] > right {
            center, right = i, i+Z[i]
        }
    }

    return Z
}

func countSubstrings(s string) int {
    sum := 0

    for _, v := range manach(s) {
        sum += (v+1)/2
    }

    return sum
}

// [1219] Path with Maximum Gold
func gridStep(grid [][]int, x int, y int) int {
    if x<0 || y<0 || y>=len(grid) || x >= len(grid[y]) {
        return 0
    }
    val := grid[y][x]
    if val == 0 {
        return 0
    }
    max := val
    grid[y][x] = 0
    step_left := gridStep(grid, x-1, y)
    step_right := gridStep(grid, x+1, y)
    step_up := gridStep(grid, x, y-1)
    step_down := gridStep(grid, x, y+1)
    grid[y][x] = val

    m := 0
    for i, e := range []int{step_left, step_right, step_up, step_down} {
        if i==0 || e > m {
            m = e
        }
    }
    return max+m
}

func getMaximumGold(grid [][]int) int {
    max := 0
    for y:=0; y < len(grid); y++ {
        for x:=0; x < len(grid[y]); x++ {
            m := gridStep(grid, x, y)
            if m > max { max = m }
        }
    }
    return max
}

// [ 2 ] Add Two Numbers
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
    result_head := &ListNode{}
    result := result_head
    carry := 0

    for (l1 != nil) || (l2 != nil) || (carry > 0) {
        out := 0
        if l1 != nil {
            out += l1.Val
            l1 = l1.Next
        }
        if l2 != nil {
            out += l2.Val
            l2 = l2.Next
        }
        out += carry
        if out >= 10 {
            out -= 10
            carry = 1
        }else {
            carry = 0
        }
        result.Val = out
        if (l1 != nil) || (l2 != nil) || carry > 0 {
            result.Next = &ListNode{}
            result = result.Next
        }
    }

    return result_head
}

// [ 3 ] Longest Substring Without Repeating Characters
func lengthOfLongestSubstring(s string) int {
    var haveSeen map[rune]int
    max_len := 0
    cur_len := 0
    i := 0

    haveSeen = make(map[rune]int)

    for j, c := range s {
        pos, seen := haveSeen[c]
        if ! seen {
            haveSeen[c] = j
        }else {
            if pos >= i { i = pos + 1 }
            haveSeen[c] = j
        }
        cur_len = j - i + 1
        if cur_len > max_len {
            max_len = cur_len
        }
    }
    return max_len
}

// [ 519 ] Random Flip Matrix
import "math/rand"

type Solution struct {
    n_rows int
    n_cols int
    values [][]int
    z_count []int
    z_total int
}


func Constructor(n_rows int, n_cols int) Solution {
    Sol := new(Solution)

    Sol.n_rows = n_rows
    Sol.n_cols = n_cols
    Sol.z_total = n_rows*n_cols
    Sol.z_count = make([]int, n_rows)
    for i:=0; i<n_rows; i++ { Sol.z_count[i] = n_cols }
    Sol.values = make([][]int, n_rows)
    for i:=0; i<n_rows; i++ { Sol.values[i] = make([]int, n_cols) }

    return *Sol
}


func (this *Solution) Flip() []int {
    pos := rand.Intn(this.z_total)
    row, col := 0, pos
    for col >= this.z_count[row] && row < this.n_rows {
        col -= this.z_count[row]
        row++
    }

    this.z_total--
    this.z_count[row]--
    z_pos := 0
    for j:=0; j < this.n_cols; j++ {
        if this.values[row][j] == 0 {
            if col > 0 {
                col--
            }else {
                z_pos = j
                break
            }
        }
    }

    this.values[row][z_pos] = 1
    return []int{row, z_pos}
}


func (this *Solution) Reset()  {
    for row := 0; row < this.n_rows && this.z_total < this.n_rows*this.n_cols; row ++ {
        if this.z_count[row] == this.n_cols { continue }
        this.z_total += (this.n_cols - this.z_count[row])
        this.z_count[row] = this.n_cols
        for j:=0; j<this.n_cols; j++ {
            this.values[row][j] = 0
        }
    }
}


/**
 * Your Solution object will be instantiated and called as such:
 * obj := Constructor(n_rows, n_cols);
 * param_1 := obj.Flip();
 * obj.Reset();
 */

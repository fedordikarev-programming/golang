type Coord struct {
    X int
    Y int
}

const WATER = byte('0')
const LAND = byte('1')
const SEEN = byte('*')

func markIsland(grid [][]byte, curIsland []Coord) {
    H := len(grid)
    if (H < 1) { return; }
    W := len(grid[0])
    if (W < 1) { return; }

    var nextStepIsland []Coord
    for len(curIsland) > 0 {
        nextStepIsland = make([]Coord, 0)
        for _, field := range(curIsland) {
            if ((field.X > 0) && (grid[field.Y][field.X-1] == LAND)) {
                nextStepIsland = append(nextStepIsland, Coord{field.X-1, field.Y})
                grid[field.Y][field.X-1] = SEEN
            }
            if ((field.X < W-1) && (grid[field.Y][field.X+1] == LAND)) {
                nextStepIsland = append(nextStepIsland, Coord{field.X+1, field.Y})
                grid[field.Y][field.X+1] = SEEN
            }
            if ((field.Y > 0) && (grid[field.Y-1][field.X] == LAND)) {
                nextStepIsland = append(nextStepIsland, Coord{field.X, field.Y-1})
                grid[field.Y-1][field.X] = SEEN
            }
            if ((field.Y < H-1) && (grid[field.Y+1][field.X] == LAND)) {
                nextStepIsland = append(nextStepIsland, Coord{field.X, field.Y+1})
                grid[field.Y+1][field.X] = SEEN
            }
        }
        curIsland = nextStepIsland
    }
}

func numIslands(grid [][]byte) int {
    islandsCount := 0 // Lets start with 2 to distinguish from "1" for land
    

    X, Y := 0, 0
    H, W := len(grid), len(grid[0])
    for Y = 0; Y < H; Y++ {
        for X = 0; X < W; X++ {
            if (grid[Y][X] == LAND) {
                currentIsland := make([]Coord, 0)
                currentIsland = append(currentIsland, Coord{X, Y})
                grid[Y][X] = SEEN
                islandsCount++
                markIsland(grid, currentIsland)
            }
        }
    }

    return islandsCount
    
}
